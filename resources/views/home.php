<html lang="en">
<head>
    <title>
        123 Inc. - CRM Website 1.0
    </title>

    <meta charset="UTF-8">
    <link href="<?php asset('assets/css/styles.css')?>" rel="stylesheet" type="text/css">
</head>
<body>
<div class="container">
    <div class="banner">
        123 Inc CRM APP
    </div>

    <form id="loginForm" method="post" action="<?php e(route('login')) ?>">
        <div class="form-row">
            <label for="email">
                Email:
            </label>

            <input class="form-input" type="email" name="email" id="email" required autocomplete="off">
        </div>

        <div class="form-row">
            <label for="password">
                Password:
            </label>

            <input class="form-input" type="password" name="password" id="password" required autocomplete="off">
        </div>

        <input class="form-submit" type="submit" value="Log In">
    </form>
</body>
</html>