<html lang="en">
<head>
    <title>
        123 Inc. - Clients
    </title>

    <meta charset="UTF-8">
    <link href="<?php asset('assets/css/styles.css')?>" rel="stylesheet" type="text/css">
</head>
<body>
<div class="container">
    <div class="banner">
        123 Inc. CRM APP
    </div>

    <div class="menu">
        <div class="user-name">
            Welcome, <?php echo $user['first_name'] . ' ' . $user['last_name']; ?>!
        </div>

        <a href="<?php e(route('logout')) ?>" class="button logout">
            Logout
        </a>
    </div>

    <div class="users-container">
        <div class="users-container-controls">
            <a href="<?php e(route('editClient'))?>" class="button add-client">
                Add Client
            </a>

            <form method="get" action="" id="clientsListForm">
                <label for="search"></label>

                <input type="text" name="search" id="search" value="<?php e($request->get('search') ?? '')?>" autocomplete="off">

                <input type="submit" value="Search" class="button">
            </form>
        </div>

        <table class="clients-table">
            <tr>
                <th>
                    ID
                </th>
                <th>
                    Name
                </th>
                <th>
                    Email
                </th>
                <th>
                    Actions
                </th>
            </tr>

            <?php if ($clients): ?>
                <?php while ($client = $clients->fetch_assoc()): ?>
                    <tr>
                        <td><?php echo $client['id'] ?></td>
                        <td><?php echo $client['first_name'] . ' ' . $client['last_name'] ?></td>
                        <td><?php echo $client['email'] ?></td>
                        <td>
                            <div class="table-buttons">
                                <a href="<?php e(route('editClient', ['id' => $client['id']]))?>" class="button edit-button">
                                    Edit
                                </a>

                                <a href="<?php e(route('deleteClient', ['id' => $client['id'], 'search' => $request->get('search') ?? '']))?>" class="button delete-button">
                                    Delete
                                </a>
                            </div>
                        </td>
                    </tr>
                <?php endwhile; ?>
            <?php endif; ?>
        </table>
    </div>

    <script>
        (function () {
            document.querySelectorAll('.button.delete-button').forEach(function (button) {
                button.addEventListener('click', function (event) {
                    if (!confirm('Delete client?')) {
                        event.preventDefault();
                    }
                });
            })
        })();
    </script>
</body>
</html>