<html lang="en">
<head>
    <title>
        123 Inc. - <?php e($action)?> Management
    </title>

    <meta charset="UTF-8">
    <link href="<?php asset('assets/css/styles.css')?>" rel="stylesheet" type="text/css">
</head>
<body>
<div class="container">
    <div class="banner">
        123 Inc. CRM APP - <?php e($action)?> Client
    </div>

    <div class="menu">
        <a href="<?php e(route('admin')) ?>" class="button cancel">
            Cancel
        </a>
    </div>

    <div class="user-container">
        <form method="post" action="" id="updateClientForm">
            <input
                type="hidden"
                name="hiddenId"
                id="hiddenId"
                value="<?php e($client['id'] ?? '')?>"
                autocomplete="off"
                readonly
            >

            <div class="form-row">
                <label for="id">ID:</label>

                <input
                    type="text"
                    name="id"
                    id="id"
                    value="<?php e($client['id'] ?? '')?>"
                    autocomplete="off"
                    readonly
                    disabled
                >
            </div>

            <div class="form-row">
                <label for="firstName">First Name:</label>

                <input
                    type="text"
                    name="firstName"
                    id="firstName"
                    value="<?php e($client['first_name'] ?? '')?>"
                    autocomplete="off"
                    required
                >
            </div>

            <div class="form-row">
                <label for="lastName">Last Name:</label>

                <input
                    type="text"
                    name="lastName"
                    id="lastName"
                    value="<?php e($client['last_name'] ?? '')?>"
                    autocomplete="off"
                    required
                >
            </div>

            <div class="form-row">
                <label for="email">Email:</label>

                <input
                    type="email"
                    name="email"
                    id="email"
                    value="<?php e($client['email'] ?? '')?>"
                    autocomplete="off"
                    required
                >
            </div>

            <input type="submit" value="<?php e($action)?> Client" class="button" id="clientSubmit">
        </form>
    </div>

    <script>
        (function () {
            document.getElementById('clientSubmit').addEventListener('click', function (event) {
                if (!confirm('<?php e($action)?> client?')) {
                    event.preventDefault();
                }
            });
        })();
    </script>
</body>
</html>