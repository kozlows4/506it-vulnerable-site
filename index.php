<?php

use app\Lib\App;

require __DIR__ . '/vendor/autoload.php';
require __DIR__ . '/app/helpers.php';
require __DIR__ . '/routes/web.php';

App::run();