<?php


namespace app\Lib;

/**
 * Class Request
 * @package app\Lib
 *
 * @method mixed|null get(string $key, ?mixed $default = null)
 * @method mixed|null post(string $key, ?mixed $default = null)
 */
class Request
{
    /**
     * @const
     */
    private const ALLOWED_METHODS = [
        'get',
        'post'
    ];

    /**
     * @var array $getParams
     */
    private array $getParams;

    /**
     * @var array $postParams
     */
    private array $postParams;

    /**
     * @var string $requestMethod
     */
    private string $requestMethod;

    /**
     * Request constructor.
     */
    public function __construct()
    {
        $this->getParams = $_GET;
        $this->postParams = $_POST;

        $this->requestMethod = strtolower(
            trim($_SERVER['REQUEST_METHOD'])
        );
    }

    /**
     * @param $name
     * @param $arguments
     * @return mixed|null
     */
    public function __call($name, $arguments)
    {
        if (in_array($name, self::ALLOWED_METHODS)) {
            return $this->{$name.'Params'}[$arguments[0]] ?? ($arguments[1] ?? null);
        }

        return null;
    }
}