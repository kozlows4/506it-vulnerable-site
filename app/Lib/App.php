<?php

namespace app\Lib;

/**
 * Class App
 * @package app\Lib
 */
class App
{
    /**
     * Run the application
     */
    public static function run(): void
    {
        session_start();
//        session_unset();
//        session_destroy();

        if (!session('LAST_ACTIVITY')) {
            session('LAST_ACTIVITY', time());
        }

        if (time() - session('LAST_ACTIVITY') > 1800) {
            session_unset();
            session_destroy();
            session_start();

            Response::redirectResponse();
        } else {
            session('LAST_ACTIVITY', time());

            Router::handleRouting();
        }
    }
}