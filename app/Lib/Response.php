<?php


namespace app\Lib;

/**
 * Class Response
 * @package app\Lib
 */
class Response
{
    public const ROUTING_SUCCESS = 200;
    public const ROUTING_REDIRECT = 302;
    public const ROUTING_ERROR_FORBIDDEN = 403;
    public const ROUTING_ERROR_NOT_FOUND = 404;
    public const ROUTING_ERROR_METHOD_NOT_ALLOWED = 405;

    /**
     * @var int $status
     */
    public int $status = self::ROUTING_SUCCESS;

    /**
     * @param int $code
     * @return $this
     */
    public function status(int $code): self
    {
        $this->status = $code;
        return $this;
    }

    /**
     * @param array $data
     */
    public function toJSON($data = []): void
    {
        http_response_code($this->status);
        header('Content-Type: application/json');
        echo json_encode($data);
    }

    public static function successResponse()
    {

    }

    /**
     * @param int $errorCode
     */
    public static function errorResponse(int $errorCode): void
    {
        switch ($errorCode) {
            case self::ROUTING_ERROR_FORBIDDEN:
                header('HTTP/1.0 403 Forbidden');
                break;
            case self::ROUTING_ERROR_NOT_FOUND:
                header('HTTP/1.0 404 Not Found');
                break;
            case self::ROUTING_ERROR_METHOD_NOT_ALLOWED:
                header('HTTP/1.0 405 Method Not Allowed');
                break;
            default:
                break;
        }

        if (file_exists($fileName = 'resources/views/errors/' . $errorCode . '.php')) {
            include $fileName;
        }
    }

    /**
     * @param string|null $path
     */
    public static function redirectResponse(?string $path = null): void
    {
        session_start();

        $path = $path ?? ((isset($_SERVER['HTTPS']) ? 'https' : 'http') . '://' . $_SERVER['HTTP_HOST']);

        header('Location: ' . $path, true, self::ROUTING_REDIRECT);
    }
}