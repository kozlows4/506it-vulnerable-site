<?php


namespace app\Lib;


use mysqli;
use mysqli_result;

class DB
{
    /**
     * @var mysqli $connection
     */
    private mysqli $connection;

    /**
     * @param string $query
     * @return bool|mysqli_result
     */
    public static function query(string $query)
    {
        return (new self())->queryDb($query);
    }

    /**
     * DB constructor.
     */
    public function __construct()
    {
        $this->connection = new mysqli(
            config('DB_HOST'),
            config('DB_USERNAME'),
            config('DB_PASSWORD'),
            config('DB_NAME')
        );
    }

    /**
     * @param string $query
     * @return bool|mysqli_result
     */
    public function queryDb(string $query)
    {
        if (!$this->connection->connect_error) {
            return $this->connection->query($query);
        } else {
            return false;
        }
    }

    /**
     *
     */
    public function __destruct()
    {
        if ($this->connection) {
            $this->connection->close();
        }
    }
}