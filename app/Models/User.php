<?php


namespace app\Models;

use app\Lib\DB;
use app\Lib\Request;
use app\Lib\Response;
use mysqli_result;

/**
 * Class User
 * @package app\Models
 */
class User extends Model
{
    /**
     * @var string $table
     * @see Model::$table
     */
    protected static string $table = 'users';

    /**
     * @param Request $request
     * @return bool|mysqli_result
     */
    public static function login(Request $request)
    {
        return DB::query(
            self::getLoginQuery(
                $request->post('email'),
                $request->post('password')
            )
        );
    }

    /**
     *
     */
    public static function logout(): void
    {
        session_unset();
        session_destroy();

        Response::redirectResponse();
    }

    /**
     * @param string $email
     * @param string $password
     * @return string
     */
    private static function getLoginQuery(string $email, string $password): string
    {
        return 'SELECT * FROM users WHERE email = "' . $email . '" AND password = "' . $password . '"';
    }
}