<?php


namespace app\Models;

use app\Lib\DB;

/**
 * Class Model
 * @package app\Models
 */
class Model
{
    /**
     * @var string $table
     */
    protected static string $table = '';

    /**
     * @var string $key
     */
    protected static string $key = 'id';

    /**
     * @param int $id
     * @return bool
     */
    public static function delete(int $id): bool
    {
        return !!DB::query(
            'DELETE FROM ' . static::$table . ' WHERE ' . static::$key . ' = ' . $id . ';'
        );
    }

    /**
     * @param int $id
     * @return array|null
     */
    public static function get(int $id): ?array
    {
        $result = DB::query(
            'SELECT * FROM ' . static::$table . ' WHERE ' . static::$key . ' = ' . $id . ';'
        );

        if ($result && $result->num_rows > 0) {
            return $result->fetch_assoc();
        } else {
            return null;
        }
    }

    /**
     * @param array $params
     * @return bool
     */
    public static function insertUpdate(array $params): bool
    {
        if (empty($key = $params[static::$key]) || empty(static::get($key))) {
            return static::insert($params);
        } else {
            return static::update($params, $key);
        }
    }

    /**
     * @param array $params
     * @return bool
     */
    private static function insert(array $params): bool
    {
        $keys = self::joinValues(array_keys($params), false);
        $values = self::joinValues(array_values($params));

        return !!DB::query(
            'INSERT INTO ' . static::$table . ' (' . $keys . ') VALUES ('. $values .');'
        );
    }

    /**
     * @param array $params
     * @param int $id
     * @return bool
     */
    private static function update(array $params, int $id): bool
    {
        return !!DB::query(
            'UPDATE ' . static::$table . ' SET ' . self::joinKeyValuePairs($params, [static::$key]) . ' WHERE ' . static::$key . ' = ' . $id . ';'
        );
    }

    /**
     * @param array $values
     * @param bool $addQuotes
     * @param string $glue
     * @return string
     */
    private static function joinValues(array $values, bool $addQuotes = true, string $glue = ', '): string
    {
        if ($addQuotes) {
            foreach ($values as &$value) {
                if (is_null($value) || empty($value)) {
                    $value = 'NULL';
                } elseif (gettype($value) === 'string') {
                    $value = '\'' . $value . '\'';
                }
            }
        }

        return implode($glue, $values);
    }

    /**
     * @param array $params
     * @param array $excludeKeys
     * @param string $glue
     * @return string
     */
    private static function joinKeyValuePairs(array $params, array $excludeKeys = [], string $glue = ', '): string
    {
        $joinedKeyValuePairs = [];

        foreach ($params as $key => $value) {
            if (in_array($key, $excludeKeys)) {
                continue;
            }

            if (is_null($value) || empty($value)) {
                $value = 'NULL';
            } elseif (gettype($value) === 'string') {
                $value = '\'' . $value . '\'';
            }

            $joinedKeyValuePairs[] = $key . '=' . $value;
        }

        return implode($glue, $joinedKeyValuePairs);
    }
}