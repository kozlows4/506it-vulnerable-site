<?php


namespace app\Models;

use app\Lib\DB;
use app\Lib\Request;
use mysqli_result;

/**
 * Class Client
 * @package app\Models
 */
class Client extends Model
{
    /**
     * @var string $table
     * @see Model::$table
     */
    protected static string $table = 'clients';

    /**
     * @param Request $request
     * @return bool|mysqli_result|null
     */
    public static function getClients(Request $request)
    {
        $result = DB::query(
            'SELECT * FROM clients ' . self::getSearchQuery($request->get('search'))
        );

        if ($result && $result->num_rows > 0) {
            return $result;
        } else {
            return null;
        }
    }

    /**
     * @param string|null $search
     * @return string
     */
    private static function getSearchQuery(?string $search = null): string
    {
        if (is_null($search)) {
            return '';
        }

        return 'WHERE id = "' . $search
            . '" OR first_name LIKE "%' . $search
            . '%" OR last_name LIKE "%' . $search
            . '%" OR email LIKE "%' . $search . '%";';
    }
}