<?php

use app\Lib\Config;
use app\Lib\Router;

if (!function_exists('e')) {
    /**
     * Display value
     *
     * @param mixed $data
     */
    function e($data): void
    {
        echo $data;
    }
}

if (!function_exists('config')) {
    /**
     * Display value
     *
     * @param string $key
     * @param mixed|null $default
     * @return mixed|null
     */
    function config(string $key, $default = null)
    {
        return Config::get($key, $default);
    }
}

if (!function_exists('asset')) {
    /**
     * Get asset path
     *
     * @param string $asset
     */
    function asset(string $asset): void
    {
        echo '/resources/' . $asset;
    }
}

if (!function_exists('url')) {
    /**
     * Get url
     *
     * @return string
     */
    function url(): string
    {
        return $_SERVER['HTTP_HOST'];
    }
}

if (!function_exists('route')) {
    /**
     * Get route
     *
     * @param string $route
     * @param array $parameters
     * @return string
     */
    function route(string $route, array $parameters = []): string
    {
        $route = (isset($_SERVER['HTTPS']) ? 'https' : 'http')
            . '://'
            . $_SERVER['HTTP_HOST']
            . (Router::findRoute($route) ?? ('/' . $route));

        $isFirstIteration = true;
        $params = '';

        foreach ($parameters as $key => $value) {
            if ($isFirstIteration) {
                $params .= '?';

                $isFirstIteration = false;
            } else {
                $params .= '&';
            }

            $params .= ($key . '=' . $value);
        }

        return $route . $params;
    }
}

if (!function_exists('session')) {
    /**
     * Get or update session
     *
     * @param string $key
     * @param mixed|null $value
     * @return mixed|null|void
     */
    function session(string $key, $value = null)
    {
        if (isset($value)) {
            $_SESSION[$key] = $value;
        } else {
            return $_SESSION[$key] ?? null;
        }
    }
}

if (!function_exists('isAuth')) {
    /**
     * Check if user is logged in
     *
     * @return bool
     */
    function isAuth(): bool
    {
        $user = session('user');

        return (!empty($user) && $user !== 'null');
    }
}

if (!function_exists('isProtectedRoute')) {
    /**
     * Check if access to the path is restricted
     *
     * @param string $path
     * @return bool
     */
    function isProtectedRoute(string $path): bool
    {
        $isProtected = false;

        foreach (config('PROTECTED_ROUTE_PREFIX') as $prefix) {
            if (strpos($path, $prefix) !== false) {
                $isProtected = true;

                break;
            }
        }

        return $isProtected;
    }
}