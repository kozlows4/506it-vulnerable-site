<?php

namespace app\Controllers;

use app\Lib\Request;
use app\Lib\Response;
use app\Models\Client;
use app\Models\User;

/**
 * Class AuthController
 * @package app\Controllers
 */
class AuthController
{
    /**
     * @param Request $request
     */
    public function login(Request $request): void
    {
        $result = User::login($request);

        if ($result && $result->num_rows > 0) {
            session('user', json_encode($result->fetch_assoc()));

            Response::redirectResponse(route('admin'));
        } else {
            Response::redirectResponse();
        }
    }

    /**
     *
     */
    public function logout(): void
    {
        User::logout();
    }

    /**
     * @param Request $request
     */
    public function viewAdmin(Request $request): void
    {
        $user = json_decode(session('user'), true);
        $clients = Client::getClients($request);

        include 'resources/views/admin/clients.php';
    }

    /**
     * @param Request $request
     */
    public function deleteClient(Request $request): void
    {
        Client::delete($request->get('id') ?? 0);

        Response::redirectResponse(
            route(
                'admin',
                empty($search = $request->get('search')) ? [] : ['search' => $search]
            )
        );
    }

    /**
     * @param Request $request
     */
    public function editClient(Request $request): void
    {
        $client = Client::get($request->get('id') ?? 0);
        $action = isset($client) ? 'Edit' : 'Add';

        include 'resources/views/admin/client.php';
    }

    /**
     * @param Request $request
     */
    public function updateClient(Request $request): void
    {
        Client::insertUpdate([
            'id' => $request->post('hiddenId'),
            'first_name' => $request->post('firstName'),
            'last_name' => $request->post('lastName'),
            'email' => $request->post('email')
        ]);

        Response::redirectResponse(
            route('admin')
        );
    }
}