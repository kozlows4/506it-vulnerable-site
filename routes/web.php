<?php

use app\Lib\Route;

Route::get('/', function () {
    include './resources/views/home.php';
})->name('root');
Route::get('/home')->redirect('root');
Route::get('/login')->redirect('root');

Route::post('/login', 'AuthController@login')->name('login');
Route::get('/logout', 'AuthController@logout')->name('logout');
Route::get('/admin', 'AuthController@viewAdmin')->name('admin');
Route::get('/admin/editClient', 'AuthController@editClient')->name('editClient');
Route::post('/admin/editClient', 'AuthController@updateClient')->name('updateClient');
Route::get('/admin/deleteClient', 'AuthController@deleteClient')->name('deleteClient');


